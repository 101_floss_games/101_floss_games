# 101 FLOSS games

### What
A project that aims to create 101 Free/Libre and Open Source games all using 
free and open source libraries, engines, frameworks and art.  

The project follows the spirit of *David H. Ahl*'s original book 
[101 BASIC Computer Games](__101 BASIC Computer Games__), but in a git
repository form.

All games are simple and basic, and are made to mimic popular small games
released for computers, arcades and consoles (i.e. pong, snake, adventure...
*all rights reserved for their respective copyright owners*)

One thing to note, this project __will not__ utilize any non free engine such
as Unity, Unreal, game maker ...etc, or utilize any tools that do not work with
or support free operating systems.  
The only exception to this is the
[Defold](https://defold.com/) engine as it's license is
[almost](https://defold.com/license/) Free/Opensource.

This project will also __not__ include instructions to produce code or binaries
that require non free platforms, build systems, IDEs, tools or libraries.
The merit is to keep this project free for all and remain as such forever.  
You are free however to explore and adapt any given instructions to work on
your favorite OS, but we apologize for not being able to provide any support.

### Why
Why to waste time and energy better put elsewhere?
well, to be honest, to do something I like and fun.
My ultimate dream is to be able to program games for all game consoles,
past, present and future. This whole project is an excuse to do just that.

Another resason, There are so many awsome FLOSS video game related libraries,
engines, tools and frameworks written in most programming languages and support
most platforms. It would be a shame not to try and use them. 

Finally, I would like to practice my basic game development skills and
techniques to finally be able to call myself a game developer.

### How
By creating 101 game projects, each game will utilze one of the available FLOSS
projects. for example, game 001, will utilize SDL2, while game 00x might utilize
SFML or Godot.

There might be cases where two or more libraries are used in a single project
for example Box2D combined with GLFW to create a simple "circus" game clone.

By default, the latest stable version of any given FLOSS project will be used.
Currently active and maintained projects are considered, will also try to use
older projects with a track record and/or active communities or forks.

Games will be created cross platform as much as possible, unless a specific
dependency is made for a specific platform only (e.x. android).

Most projects will depend on [CMake](https://cmake.org/) to produce build
files unless a given tool is not/cannot be supported by it, or depends by
default on a specific buildsystem or package such as scons, meson...etc.

Installation of tools on your favorite *iDroidFedBsdXArchWinBuntu* OS is left
up to you as an exercise. Don't let this discourage you though, if you are here
to do or learn game developement, you should be able to figure out how to
configure your development environment. Besides, each tool or dependency already
have documentation explaining how to get started and install it's requirements 
on all supported platforms.

### List of games

The following is a list of planed games to be developed and their status.
If you have a comment, request, or a question please open an issue and I
will try to answer as quick as possible.

| Number | name | Description | projects used | languages | platform | status |
| ------ | ---- | ----------- | ------------- | --------- | -------- |--------|
| 001 | Battle | An [Air-Sea Battle](https://en.wikipedia.org/wiki/Air-Sea_Battle) clone| [SDL2](https://www.libsdl.org/download-2.0.php)| c++ | Linux, Windows, MAC | started |
| xxx | name | N/A | [SFML](https://www.sfml-dev.org/) | c++ | os | N/A |
| xxx | name | N/A | [Allegro](https://liballeg.org/) | c++ | os | N/A |
| xxx | name | N/A | [Cocos2dX](https://github.com/cocos2d/cocos2d-x) | c++ | os | N/A |
| xxx | name | N/A | [Tileengine](http://www.tilengine.org/) | c | os | N/A |
| xxx | name | N/A | [IrrLicht](http://irrlicht.sourceforge.net/) | c++ | os | N/A |
| xxx | name | N/A | [raylib](https://www.raylib.com/) | c++ | os | N/A |
| xxx | name | N/A | [esenthel](https://www.esenthel.com/?id=info) | c++ | os | N/A |
| xxx | name | N/A | [ClanLib](https://github.com/sphair/ClanLib) | c++ | os | N/A |
| xxx | name | N/A | [G3D](https://casual-effects.com/g3d/www/index.html) | c++ | os | N/A |
| xxx | name | N/A | [Urho3D](https://github.com/urho3d/Urho3D) | c++ | os | N/A |
| xxx | name | N/A | [o3de](https://github.com/o3de/o3de) | c++ | os | N/A |
| xxx | name | N/A | [nCine](https://ncine.github.io/) | c++ | os | N/A |
| xxx | name | N/A | [Gameplay3D](http://gameplay3d.io/) | c++ | os | N/A |
| xxx | name | N/A | [Plywood](https://plywood.arc80.com/) | c++ | os | N/A |
| xxx | name | N/A | [Panda3D](https://www.panda3d.org/) | c++ | os | N/A |
| xxx | name | N/A | [Oxygine](https://oxygine.org/) | c++ | os | N/A |
| xxx | name | N/A | [WickedEngine](https://github.com/turanszkij/WickedEngine) | c++ | os | N/A |
| xxx | name | N/A | [OpenWatcom2](https://github.com/open-watcom) | c | dos | N/A |
| xxx | name | N/A | [devkitArm](https://github.com/devkitPro/pacman-packages) [flecs](https://github.com/SanderMertens/flecs) | c++ | gba | N/A |
| xxx | name | N/A | [devkitArm](https://github.com/devkitPro/pacman-packages) | c++ | nds | N/A |
| xxx | name | N/A | [kos](http://cadcdev.sourceforge.net/softprj/kos/) | ?? | dc | N/A |
| xxx | name | N/A | [devkitArm](https://github.com/devkitPro/pacman-packages) | c++ | psp | N/A |
| xxx | name | N/A | [cc65](https://cc65.github.io/) | as | nes | N/A |
| xxx | name | N/A | [pspdev](https://github.com/pspdev) | ?? | nes | N/A |
| xxx | name | N/A | [Delta3D](https://github.com/delta3d/delta3d/) | c++ | os | N/A |
| xxx | name | N/A | [Ogre](https://www.ogre3d.org/) | c++ | os | N/A |
| xxx | name | N/A | [Ogre-Next](https://www.ogre3d.org/) | c++ | os | N/A |
| xxx | name | N/A | [OSG](http://www.openscenegraph.com/) | c++ | os | N/A |
| xxx | name | N/A | [VSG](https://github.com/vsg-dev/VulkanSceneGraph) | c++ | os | N/A |
| xxx | name | N/A | [OpenBOR](https://github.com/DCurrent/openbor) | c++ | os | N/A |
| xxx | name | N/A | [snax](https://github.com/snaxgameengine/snax) | c++ | os | N/A |
| xxx | name | N/A | [Rootex](https://github.com/sdslabs/Rootex) | c++ | os | N/A |
| xxx | name | N/A | [Spartan](https://github.com/PanosK92/SpartanEngine) | c++ | os | N/A |
| xxx | name | N/A | [Diligent](https://github.com/DiligentGraphics/DiligentEngine) | c++ | os | N/A |
| xxx | name | N/A | [Godot](https://godotengine.org) | gdscript | os | N/A |
| xxx | name | N/A | [defold](https://defold.com/) | lua | os | N/A |
| xxx | name | N/A | [MONOGAME](https://www.monogame.net/) | c# | os | N/A |
| xxx | name | N/A | [Phaser](https://github.com/photonstorm/phaser) | js | os | N/A |
| xxx | name | N/A | [FNA](https://fna-xna.github.io/) | c# | os | N/A |
| xxx | name | N/A | [LibGDX](https://libgdx.com/) | java | os | N/A |
| xxx | name | N/A | [love2d](https://love2d.org/) | lua | os | N/A |
| xxx | name | N/A | [pygame](https://www.pygame.org/news) | python | os | N/A |
| xxx | name | N/A | [GB studio](https://www.gbstudio.dev/) | ?? | os | N/A |
| xxx | name | N/A | [TIC-80](https://tic80.com/) | lua | os | N/A |
| xxx | name | N/A | [microStudio](https://microstudio.dev/) | ?? | os | N/A |
| xxx | name | N/A | [BEVY](https://bevyengine.org/) | rust | os | N/A |
| xxx | name | N/A | [Heaps](https://heaps.io/about.html) | haxe | os | N/A |
| xxx | name | N/A | [JMonkey](https://jmonkeyengine.org/) | java | os | N/A |
| xxx | name | N/A | [BabylonJS](https://www.babylonjs.com/) | js | os | N/A |
| xxx | name | N/A | [GDevelop](https://github.com/4ian/GDevelop) | ?? | os | N/A |
| xxx | name | N/A | [NeoAxis](https://www.neoaxis.com/) | c# | os | N/A |
| xxx | name | N/A | [melonJS](https://melonjs.org/) | js | os | N/A |
| xxx | name | N/A | [Stride](https://www.stride3d.net/) | c# | os | N/A |
| xxx | name | N/A | [PixiJS](https://www.pixijs.com/) | js | os | N/A |
| xxx | name | N/A | [lwjgl](https://www.lwjgl.org/) | java | os | N/A |
| xxx | name | N/A | [three.js](https://threejs.org) | js | os | N/A |
| xxx | name | N/A | [ct.js](https://ctjs.rocks/) | js | os | N/A |
| xxx | name | N/A | [twine](https://twinery.org/) | ?? | os | N/A |
| xxx | name | N/A | [flatredball](https://flatredball.com/) | c# | os | N/A |
| xxx | name | N/A | [PlayCanvas](https://playcanvas.com/) | js | os | N/A |
| xxx | name | N/A | [Arcade Library](https://api.arcade.academy/en/latest/) | python | os | N/A |
| xxx | name | N/A | [Amulet](https://www.amulet.xyz/) | lua | os | N/A |
| xxx | name | N/A | [Ursina](https://www.ursinaengine.org/) | python | os | N/A |
| xxx | name | N/A | [openfl](https://www.openfl.org/) | haxe | os | N/A |
| xxx | name | N/A | [Crafty](https://github.com/craftyjs/Crafty) | js | os | N/A |
| xxx | name | N/A | [HaxeFlixel](https://haxeflixel.com/) | haxe | os | N/A |
| xxx | name | N/A | [Gideros](http://giderosmobile.com/download) | lua | os | N/A |
| xxx | name | N/A | [matter-js](https://brm.io/matter-js/) | js | os | N/A |
| xxx | name | N/A | [PixelBox](https://github.com/cstoquer/pixelbox) | js | os | N/A |
| xxx | name | N/A | [Taro](https://github.com/Cloud9c/taro) | js | os | N/A |
| xxx | name | N/A | [orx](https://orx-project.org/) | ?? | os | N/A |
| xxx | name | N/A | [lychee.js](https://github.com/cookiengineer/lycheejs) | ?? | os | N/A |
| xxx | name | N/A | [Bear](https://github.com/j-jorge/bear) | c++ | os | N/A |
| xxx | name | N/A | [Echo](https://github.com/timi-liuliang/echo) | ?? | os | N/A |
| xxx | name | N/A | [BT Builder](https://github.com/dulsi/btbuilder) | c++ | os | N/A |
| xxx | name | N/A | [Flare](https://flarerpg.org/) | c++ | os | N/A |
| xxx | name | N/A | [FreeDink](https://www.gnu.org/software/freedink/) | ?? | os | N/A |
| xxx | name | N/A | [Stratagus](https://github.com/Wargus/Stratagus) | ?? | os | N/A |
| xxx | name | N/A | [cannon-es](https://github.com/pmndrs/cannon-es) | js | os | N/A |
| xxx | name | N/A | [T-Engine4](https://te4.org/te4) | lua | os | N/A |
| xxx | name | N/A | [Lumix](https://github.com/nem0/lumixengine) | ?? | os | N/A |
| xxx | name | N/A | [radius](http://radius-engine.sourceforge.net/) | ?? | os | N/A |
| xxx | name | N/A | [Ren'Py](https://renpy.org/) | python | os | N/A |
| xxx | name | N/A | [Flame](https://flame-engine.org/) | dart | os | N/A |
| xxx | name | N/A | [Ultraviolet](https://github.com/tlgkccampbell/ultraviolet) | c# | os | N/A |
| xxx | name | N/A | [nunustudio](https://www.nunustudio.org/) | js | os | N/A |
| xxx | name | N/A | [Fyrox](https://fyrox.rs/) | rust | os | N/A |
| xxx | name | N/A | [Overgrowth](https://github.com/WolfireGames/overgrowth) | c++ | os | N/A |
| xxx | name | N/A | [BRender](https://github.com/foone/BRender-v1.3.2) | c++ | os | N/A |
| xxx | name | N/A | [KorGE](https://korge.org/) | kotlin | os | N/A |
| xxx | name | N/A | [dragonengine](https://github.com/LordOfDragons/dragengine) | c++ | os | N/A |
| xxx | name | N/A | [Castle Game Engine](https://castle-engine.io/) | pascal | os | N/A |
| xxx | name | N/A | [exEngine](https://github.com/ezEngine/ezEngine) | c++ | os | N/A |
| xxx | name | N/A | [MINIGDX](https://github.com/minigdx/minigdx) | kotlin | os | N/A |

# Copyright
Part of the __101 FLOSS games project__

*2022 Mohammad Rasmi Khashashneh* 

## License
```
Battle

The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
he rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
```
